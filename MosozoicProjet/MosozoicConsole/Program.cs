﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MosozoicConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Dinosaur louis, nessie, bob, sully;
            louis = new Dinosaur("Louis", "Stegausaurus", 12);
            nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            bob = new Dinosaur("Bob", "Raptor Jesus", 24);
            sully = new Dinosaur("Sully", "T-rex", 7);

            Console.WriteLine("Présentation de nos dinosaures:");

            Console.WriteLine(louis.sayHello());
            Console.WriteLine(nessie.sayHello());
            Console.WriteLine(bob.sayHello());
            Console.WriteLine(sully.sayHello());


            Console.WriteLine("\nCréation d'un troupeau");

            Horde horde = new Horde();
            horde.AddDinosaur(louis);
            horde.AddDinosaur(nessie);
            horde.AddDinosaur(bob);
            horde.AddDinosaur(sully);

            Console.WriteLine(horde.IntroduceAll());

            Console.ReadKey();
        }
    }
}
