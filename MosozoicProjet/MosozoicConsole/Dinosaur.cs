﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MosozoicConsole
{
    public class Dinosaur
    {
        private string name;
        private string specie;
        private int age;


        public Dinosaur(string name, string specie, int age)
        {
            this.name = name;
            this.specie = specie;
            this.age = age;
        }

        public string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, this.specie, this.age);
        }

        public string roar()
        {
            return "Grrr";
        }

        public string Hug(Dinosaur dinosaur)
        {
            if (dinosaur == this)
            {
                return String.Format("Je suis {0} et je ne peux pas me faire de câlin à moi-même :'(.", this.name);
            }
            return String.Format("Je suis {0} et je fais un câlin à {1}.", this.name, dinosaur.GetName());
        }

        public string GetName()
        {
            return this.name;
        }

        public string GetSpecie()
        {
            return this.specie;
        }

        public int GetAge()
        {
            return this.age;
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public void SetSpecie(string specie)
        {
            this.specie = specie;
        }

        public void SetAge(int age)
        {
            this.age = age;
        }
    }
}